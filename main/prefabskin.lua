--[WARNING]: This file is imported into modclientmain.lua, be careful!

local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local _basic_init_fn = basic_init_fn
function basic_init_fn(inst, build_name, def_build, ...)
    if inst and inst.components.visualvariant then
        inst.components.visualvariant:Set()
    end
    return _basic_init_fn(inst, build_name, def_build, ...)
end

--machete_init_fn = function(inst, build_name) basic_init_fn( inst, build_name, "machete" ) end
--machete_clear_fn = function(inst) basic_clear_fn(inst, "machete" ) end