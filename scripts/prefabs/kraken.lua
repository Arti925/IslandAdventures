
local assets =
{
    Asset("ANIM", "anim/quacken.zip"),
}

local prefabs =
{
    "kraken_tentacle",
    "kraken_projectile",
    "kraken_inkpatch",
    "krakenchest",
}

SetSharedLootTable('kraken',
{
    {"piratepack", 1.00},
})

local MIN_HEALTH =
{
	0.75,
	0.50,
	0.25,
	-1.0,
}

local MaxHealth = TUNING.QUACKEN_HEALTH

local function MoveToNewSpot(inst)
	local pos = inst:GetPosition()
	local new_pos = pos
	for i=1, 50 do
		local offset = FindWaterOffset(pos, math.pi * 2 * math.random(), 40, 30)
		new_pos = pos + offset
		local tile = TheWorld.Map:GetTileAtPoint(new_pos:Get())
		if tile == WORLD_TILES.OCEAN_DEEP or tile == WORLD_TILES.OCEAN_SHIPGRAVEYARD  then
			break
		end
    end
	inst:PushEvent("move", {pos = new_pos})
end

local function OnMinHealth(inst, data)
    if not inst.components.health:IsDead() then
        inst.health_stage = inst.health_stage + 1
        inst.health_stage = math.min(inst.health_stage, #MIN_HEALTH)
        inst.components.health:SetMinHealth(MaxHealth * MIN_HEALTH[inst.health_stage])
        MoveToNewSpot(inst)
    end
end

local RND_OFFSET = 10

local function OnAttack(inst, data)
	local numshots = TUNING.QUACKEN_PROJECTILE_COUNT

	if data.target then
		for i = 1, numshots do
			local offset = Vector3(math.random(-RND_OFFSET, RND_OFFSET), math.random(-RND_OFFSET, RND_OFFSET), math.random(-RND_OFFSET, RND_OFFSET))
			inst.components.thrower:Throw(data.target:GetPosition() + offset)
		end
	end
end

local CAN_TAGS = {"prey"}
local ONOF_TAGS = {"character", "monster", "animal"}
local function Retarget(inst)
    return FindEntity(inst, 40, function(guy)
        if guy.components.combat and guy.components.health and not guy.components.health:IsDead() then
            return not (guy.prefab == inst.prefab)
        end
    end, nil, CAN_TAGS, ONOF_TAGS)
end

local function ShouldKeepTarget(inst, target)
    if target and target:IsValid() and target.components.health and not target.components.health:IsDead() then
        local distsq = target:GetDistanceSqToInst(inst)
        return distsq < 1600
    else
        return false
    end
end

local function SpawnChest(inst)
    inst:DoTaskInTime(3, function()
		inst.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")

		local chest = SpawnPrefab("krakenchest")
		local pos = inst:GetPosition()
		chest.Transform:SetPosition(pos.x, 0, pos.z)

		local fx = SpawnPrefab("statue_transition_2")
		if fx then
			fx.Transform:SetPosition(inst:GetPosition():Get())
			fx.Transform:SetScale(1, 2, 1)
		end

		fx = SpawnPrefab("statue_transition")
		if fx then
			fx.Transform:SetPosition(inst:GetPosition():Get())
			fx.Transform:SetScale(1, 1.5, 1)
		end

		chest:AddComponent("scenariorunner")
		chest.components.scenariorunner:SetScript("chest_kraken")
		chest.components.scenariorunner:Run()
	end)
end

local function OnRemove(inst)
    inst.components.minionspawner:DespawnAll()
end

local function OnSave(inst, data)
    data.health_stage = inst.health_stage
end

local function OnLoad(inst, data)
    if data and data.health_stage then
        inst.health_stage = data.health_stage or inst.health_stage
        inst.components.health:SetMinHealth(MaxHealth * MIN_HEALTH[inst.health_stage])
    end
end

------------------------minionspawner change --------------------------
local function AddPosition(self, num)
	table.insert(self.freepositions, num)
	table.sort(self.freepositions)
end

local function OnLostMinion(self, minion)
	if self.minions[minion] == nil then
		return
	end

	self:AddPosition(minion.minionnumber)

	self.minions[minion] = nil
	self.numminions = self.numminions - 1

	self.inst:RemoveEventCallback("attacked", self._onminionattacked, minion)
	self.inst:RemoveEventCallback("onattackother", self._onminionattack, minion)
	self.inst:RemoveEventCallback("death", self._onminiondeath, minion)
	self.inst:RemoveEventCallback("onremove", self._onminionremoved, minion)

	self.inst:PushEvent("minionchange")

	if self.shouldspawn and not self:MaxedMinions() then
		self:StartNextSpawn()
	end
end

local function SpawnNewMinion(self, force)
    if self.minionpositions == nil then
        self.minionpositions = self:MakeSpawnLocations()
        if self.minionpositions == nil then
            return
        end
    end

    if (force or self.shouldspawn) and not self:MaxedMinions() and #self.freepositions > 0 then
        self.spawninprogress = false

        local num = self.freepositions[math.random(#self.freepositions)]
        local pos = self:GetSpawnLocation(num)
        if pos ~= nil then
            local minion = self:MakeMinion()
            if minion ~= nil then
                minion.sg:GoToState("spawn")
                minion.minionnumber = num
                self:TakeOwnership(minion)
                minion.Transform:SetPosition(pos:Get())
                self:RemovePosition(num)

                if self.onspawnminionfn ~= nil then
                    self.onspawnminionfn(self.inst, minion)
                end
            end
        elseif self.miniontype ~= nil and not self:MaxedMinions() then
            self.minionpositions = self:MakeSpawnLocations()
        end

        if (force or self.shouldspawn) and not self:MaxedMinions() then
            self:StartNextSpawn()
        end
    end
end

-----------------------------------------------------------------------


local function fn()

	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.entity:AddMiniMapEntity()
	inst.MiniMapEntity:SetIcon("quacken.tex")
	inst.MiniMapEntity:SetPriority(4)

	MakeCharacterPhysics(inst, 1000, 1)

    anim:SetBank("quacken")
    anim:SetBuild("quacken")
    anim:PlayAnimation("idle_loop", true)

	inst:AddTag("kraken")
    inst:AddTag("nowaves")
    inst:AddTag("epic")
    inst:AddTag("noteleport")
	inst:AddTag("mudacamada")

    if not TheNet:IsDedicated() then
        inst:DoTaskInTime(1, function()
			if inst:IsNear(ThePlayer, 60) then
				ThePlayer:PushEvent("QuackenEncounter")  --for danger music on Quacken spawn
			end
        end)
    end

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inspectable")
	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(MaxHealth)
	inst.components.health.nofadeout = true

	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(0)
	inst.components.combat:SetAttackPeriod(TUNING.QUACKEN_ATTACK_PERIOD)
	inst.components.combat:SetRange(40, 50)
	inst.components.combat:SetRetargetFunction(1, Retarget)
	inst.components.combat:SetKeepTargetFunction(ShouldKeepTarget)
	inst:AddComponent("locomotor")

	inst:AddComponent("sanityaura")

	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetChanceLootTable('kraken')
	if IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) then
        inst.components.lootdropper:AddChanceLoot("winter_ornament_boss_kraken_tentacle", 1)
		inst.components.lootdropper:AddChanceLoot("winter_ornament_boss_kraken_tentacle", 1)
		inst.components.lootdropper:AddChanceLoot("winter_ornament_boss_kraken_tentacle", 1)
    end

	inst:AddComponent("thrower")
	inst.components.thrower.throwable_prefab = "kraken_projectile"

	inst:SetStateGraph("SGkraken")

    local brain = require("brains/krakenbrain")
	inst:SetBrain(brain)

	inst.health_stage = 1

    inst:ListenForEvent("minhealth", OnMinHealth)
    inst.components.health:SetMinHealth(MaxHealth * MIN_HEALTH[inst.health_stage])
    inst:ListenForEvent("death", SpawnChest)
    inst:ListenForEvent("onattackother", OnAttack)
    inst:ListenForEvent("onremove", OnRemove)

    inst:AddComponent("minionspawner")
    inst.components.minionspawner.validtiletypes = {
		[WORLD_TILES.OCEAN_SHALLOW] = true,
		[WORLD_TILES.OCEAN_MEDIUM] = true,
		[WORLD_TILES.OCEAN_DEEP] = true,
		[WORLD_TILES.OCEAN_CORAL] = true,
		[WORLD_TILES.MANGROVE] = true,
		[WORLD_TILES.OCEAN_SHIPGRAVEYARD] = true
	}
    inst.components.minionspawner.miniontype = "kraken_tentacle"
    inst.components.minionspawner.distancemodifier = TUNING.QUACKEN_TENTACLE_DIST_MOD
    inst.components.minionspawner.maxminions = TUNING.QUACKEN_MAXTENTACLES
	inst.components.minionspawner:RegenerateFreePositions()
	inst.components.minionspawner.shouldspawn = false
	inst.components.minionspawner._onminionremoved = inst.components.minionspawner._onminiondeath
	inst.components.minionspawner.AddPosition = AddPosition
	inst.components.minionspawner.OnLostMinion = OnLostMinion
	inst.components.minionspawner.SpawnNewMinion = SpawnNewMinion

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	return inst
end

return Prefab( "kraken", fn, assets, prefabs)
