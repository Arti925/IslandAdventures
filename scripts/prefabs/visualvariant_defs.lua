local POSSIBLE_VARIANTS = {}

local VISUALVARIANT_PREFABS = {}

POSSIBLE_VARIANTS.grassgekko = {
    default = {build="grassgecko"},
    tropical = {build="grassgecko_green_build",testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.grasspartfx = {
    default = {build="grass1"},
    tropical = {build="grassgreen_build"},
}

POSSIBLE_VARIANTS.grass = {
    default = {build="grass1",minimap="grass.png"},
    tropical = {build="grassgreen_build",minimap="grass_tropical.tex",testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.grass_water = {
    default = {minimap="grass.png",override={
        {"grass_pieces", "grass1", "grass_pieces"},
    }},
    tropical = {minimap="grass_tropical.tex",override={
        {"grass_pieces", "grassgreen_build", "grass_pieces"},

    },testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.dug_grass = {
    default = {build="grass1",invatlas="default"},
    tropical = {build="grassgreen_build",invatlas="images/ia_inventoryimages.xml", sourceprefabs={
        "grass_water",
    },testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.krampus = {
    default = {build="krampus_build"},
    tropical = {build="krampus_hawaiian_build",testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.butterfly = {
    default = {build="butterfly_basic",invatlas="default"},
    tropical = {build="butterfly_tropical_basic",invatlas="images/ia_inventoryimages.xml",testfn=IsInIAClimate},
}
    
POSSIBLE_VARIANTS.cutgrass = {
    default = {build="cutgrass",invatlas="default",sourceprefabs={
        "grassgator", -- Maybe make this a visualvariant?
        "tumbleweed",
    },sourcetags={
        "cavedweller",
    }},
    tropical = {build="cutgrassgreen",invatlas="images/ia_inventoryimages.xml",sourceprefabs={
        "grass_water",
    },testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.butterflywings = {
    default = {build="butterfly_wings",bank="butterfly_wings",invatlas="default"},
    tropical = {build="butterfly_tropical_wings",bank="butterfly_tropical_wings",invatlas="images/ia_inventoryimages.xml",testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.log = {
    default = {build="log",invatlas="default",sourceprefabs={
        "marsh_tree",
        "evergreen",
        "evergreen_sparse",
        "winter_tree",
        "twiggytree",
        "winter_twiggytree",
        "deciduoustree",
        "winter_deciduoustree",
        "palmconetree",
        "winter_palmconetree",
        "leif",
        "leif_sparse",
        "moon_tree",
        "oceantree",
        "oceantree_pillar",
    },sourcetags={
        "deciduoustree",
        "cavedweller",
        "mushtree",
    }},
    tropical = {build="log_tropical",invatlas="images/ia_inventoryimages.xml",sourceprefabs={
        "palmtree",
        "winter_palmtree",
        "jungletree",
        "winter_jungletree",
        "mangrovetree",
        "livingjungletree",
        "leif_palm",
        "leif_jungle",
        "wallyintro_debris_1",
        "wallyintro_debris_2",
        "wallyintro_debris_3",
    },testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.cave_banana = {
    default = {name="default",build="cave_banana",invatlas="default",sourceprefabs={
        "cave_banana_tree",
        "cave_banana_burnt",
        "cave_banana_stump",
    },sourcetags={
        "cavedweller",
    }},
    tropical = {name="BANANA",build="bananas",invatlas="images/ia_inventoryimages.xml",sourceprefabs={
        "primeape",
        "primeapebarrel",
        "jungletree",
        "bananabush", --neat
        "leif_jungle",
        "treeguard_banana",
    },testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.cave_banana_cooked = {
    default = {name="default",build="cave_banana",invatlas="default"},
    tropical = {name="BANANA_COOKED",build="bananas",invatlas="images/ia_inventoryimages.xml",testfn=IsInIAClimate},
}

POSSIBLE_VARIANTS.resurrectionstone = {
    default = {build="resurrection_stone",bank="resurrection_stone"},
    tropical = {build="resurrection_stone_sw",bank="resurrection_stone_sw",testfn=IsInIAClimate},
}

return {POSSIBLE_VARIANTS = POSSIBLE_VARIANTS, VISUALVARIANT_PREFABS = VISUALVARIANT_PREFABS}