local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
require("stategraphs/commonstates")

local function onsleep(inst)
    if inst.components.health == nil or (inst.components.health ~= nil and not inst.components.health:IsDead()) then
		if inst.sg:HasStateTag("jumping") and inst.components.drownable ~= nil and inst.components.drownable:ShouldDrown() then
			inst.sg:GoToState("sink")
		else
		    inst.sg:GoToState(inst.sg:HasStateTag("sleeping") and "sleeping" or "sleep")
		end
    end
end
--------------------------------------------------------------------------
local function sleeponanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("sleeping")
    end
end
--------------------------------------------------------------------------
local function idleonanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("idle")
    end
end
-----------------------------------------------
local function onwakeup(inst)
	if not inst.sg:HasStateTag("nowake") then
	    inst.sg:GoToState("wake")
	end
end

--in ds you can override the anims for the commonstates but you cant in dst, this is just a copy of the dst function that allows you to override the anims (used for tigershark) -Half
--honestly i could of just set the states directly in the tigershark sg...
CommonStates.AddSleepStatesWithAnim = function(states, timelines, fns, anims)
    table.insert(states, State{
        name = "sleep",
        tags = { "busy", "sleeping" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation((anims and anims.sleep_pre) or "sleep_pre")
            if fns ~= nil and fns.onsleep ~= nil then
                fns.onsleep(inst)
            end
        end,

        timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animover", sleeponanimover),
            EventHandler("onwakeup", onwakeup),
        },
    })

    table.insert(states, State{
        name = "sleeping",
        tags = { "busy", "sleeping" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation((anims and anims.sleep_loop) or "sleep_loop")
        end,

        timeline = timelines ~= nil and timelines.sleeptimeline or nil,

        events =
        {
            EventHandler("animover", sleeponanimover),
            EventHandler("onwakeup", onwakeup),
        },
    })

    table.insert(states, State{
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation((anims and anims.sleep_pst) or "sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            if fns ~= nil and fns.onwake ~= nil then
                fns.onwake(inst)
            end
        end,

        timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", idleonanimover),
        },
    })
end

local _PlayMiningFX = PlayMiningFX
function PlayMiningFX(inst, target, nosound, ...)
    if target ~= nil and target:IsValid() then
        local coral = target:HasTag("coral")
        local charcoal = target:HasTag("charcoal")
        if coral or charcoal then
            if target.Transform ~= nil then
                SpawnPrefab(
                    (charcoal and "mining_charcoal_fx") or
                    "mining_fx"
                ).Transform:SetPosition(target.Transform:GetWorldPosition())
            end
            if not nosound and inst.SoundEmitter ~= nil then
                inst.SoundEmitter:PlaySound(
                    (charcoal and "ia/common/charcoal_mine") or
                    "ia/common/coral_mine"
                )
            end
            return
        end
    end
    return _PlayMiningFX(inst, target, nosound, ...)
end