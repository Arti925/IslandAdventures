local IAENV = env
GLOBAL.setfenv(1, GLOBAL)
---------------------------------

IAENV.AddComponentPostInit("flotsamgenerator", function(cmp)
    local _map = TheWorld.Map
    local _worldstate = TheWorld.state

    local _rebatchtime = TUNING.FLOTSAM_REBATCH_TIME
    local _batchremaining = 0

    local _spawnradius = TUNING.FLOTSAM_SPAWN_RADIUS
    local _batchsize = TUNING.FLOTSAM_BATCH_SIZE

    if TheWorld:HasTag("forest") then

        local function GetRainModifier()
            -- flotsam batches are more likely when it's raining ("storms!")
            return 1 + _worldstate.precipitationrate * 15
        end
        
        local _OnUpdate = cmp.OnUpdate
        function cmp:OnUpdate(dt, ...)

            _rebatchtime = _rebatchtime - dt * GetRainModifier()

            if _rebatchtime <= 0 then
                _rebatchtime = TUNING.FLOTSAM_REBATCH_TIME
                _batchremaining = _batchremaining + _batchsize.min + math.random(_batchsize.max - _batchsize.min)
            end

            if _OnUpdate ~= nil then
                return _OnUpdate(self, dt, ...)
            end
        end
        cmp.inst:StartUpdatingComponent(cmp)

        local _OnSave = cmp.OnSave
        function cmp:OnSave(...)
            local data = {_OnSave(self, ...)}
            if data[1] ~= nil then
                data[1].rebatchtime = _rebatchtime
                data[1].batchremaining = _batchremaining
                --useless but flotgen saves them too so do the same for consistancy?
                data[1].spawnradius = _spawnradius
                data[1].batchsize = _batchsize
            end
            return unpack(data)
        end

        local _OnSave = cmp.OnLoad
        function cmp:OnLoad(data, ...)
            if data ~= nil then
                _rebatchtime = data.rebatchtime or TUNING.FLOTSAM_REBATCH_TIME
                _batchremaining = data.batchremaining or 0
                _spawnradius = data.spawnradius or TUNING.FLOTSAM_SPAWN_RADIUS
                _batchsize = data.batchsize or TUNING.FLOTSAM_BATCH_SIZE
            end
            return unpack(data)
        end

    end

    local function flotsam_GetSpawnPoint(pt--[[,platform--]])
        local function TestOffset(offset)
            local pos = pt + offset
            return IsOceanTile(GetGroundTypeAtPosition(pos)) and _map:IsSurroundedByWater(pos.x, pos.y, pos.z, 6)
        end

        local theta = math.random() * 360
        local resultoffset = FindValidPositionByFan(theta, _spawnradius, 20, TestOffset)

        if resultoffset ~= nil then
            return pt + resultoffset
        end
    end

    local function flotsam_spawn(player, reschedule, override_prefab, override_notrealflotsam, ...)
        local flotsam = nil

        if _batchremaining > 0 and IsInDSTClimate(player) and not player:IsOnOcean(true) then
            local pt = player:GetPosition()
            local spawnpoint = flotsam_GetSpawnPoint(pt--[[, player:GetCurrentPlatform()--]])
            if spawnpoint ~= nil then
                flotsam = SpawnPrefab(override_prefab)
                flotsam.Physics:Teleport(spawnpoint:Get())
                flotsam.components.drifter:SetDriftTarget(pt)
                if flotsam.debris ~= nil then
                    for i,debris in pairs(flotsam.debris) do
                        if debris.components.spawnfader ~= nil then
                            debris.components.spawnfader:FadeIn()
                        end
                    end
                end
            end
            _batchremaining = _batchremaining - 1
        end

        return flotsam
    end

    local _guaranteed_presets
    local guaranteed_presets = {
        flotsam = { prefabs = { "flotsam" }, rate = TUNING.FLOTSAM_INDIVIDUAL_TIME, variance = TUNING.FLOTSAM_INDIVIDUAL_VARIANCE, spawnfn = flotsam_spawn },
    }

    local _scheduledtasks
    local _SpawnFlotsamForPlayer
    local function SpawnFlotsamForPlayer(player, reschedule, override_prefab, override_notrealflotsam, ...)
        local preset = _guaranteed_presets[override_prefab]
        if preset ~= nil and preset.spawnfn ~= nil then
            local flotsam = preset.spawnfn(player, reschedule, override_prefab, override_notrealflotsam, ...)
            if reschedule ~= nil then
                if _scheduledtasks ~= nil then
                    _scheduledtasks[player] = nil
                end
                reschedule(player)
            end
            return flotsam
        else
            return _SpawnFlotsamForPlayer ~= nil and _SpawnFlotsamForPlayer(player, reschedule, override_prefab, override_notrealflotsam, ...)
        end
    end

    for i, v in ipairs(cmp.inst.event_listening["ms_playerjoined"][TheWorld]) do
        if UpvalueHacker.GetUpvalue(v, "StartGuaranteedSpawn") then
            _guaranteed_presets =  UpvalueHacker.GetUpvalue(v, "StartGuaranteedSpawn", "guaranteed_presets")
            for i,v in pairs(guaranteed_presets) do
                _guaranteed_presets[i] = v
            end
            _SpawnFlotsamForPlayer = UpvalueHacker.GetUpvalue(v, "ScheduleSpawn", "SpawnFlotsamForPlayer")
            UpvalueHacker.SetUpvalue(v, SpawnFlotsamForPlayer, "ScheduleSpawn", "SpawnFlotsamForPlayer")
            _scheduledtasks = UpvalueHacker.GetUpvalue(v, "ScheduleSpawn", "SpawnFlotsamForPlayer", "_scheduledtasks")
            --reset
            cmp:ToggleUpdate()
            break
        end
    end
end)
