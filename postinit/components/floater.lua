local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Floater = require("components/floater")

function Floater:UpdateAnimations(water_anim, land_anim)
    self.wateranim = water_anim or self.wateranim
    self.landanim = land_anim or self.landanim

    if self.showing_effect then
        self:PlayWaterAnim()
    else
        self:PlayLandAnim()
    end
end

function Floater:PlayLandAnim()
    local anim = self.landanim
    if anim == "noanim" then return end
    if anim and type(anim) == "function" then
        anim = self.landanim(self.inst)
    end

	if anim and not self.inst.AnimState:IsCurrentAnimation(anim) then
        self.inst.AnimState:PlayAnimation(anim, true)
    end

	self.inst.AnimState:SetLayer(LAYER_WORLD)
	self.inst.AnimState:SetSortOrder(0)
    self.inst.AnimState:OverrideSymbol("water_ripple", "ripple_build", "water_ripple")
    self.inst.AnimState:OverrideSymbol("water_shadow", "ripple_build", "water_shadow")
end

function Floater:PlayWaterAnim()
    local anim = self.wateranim
    if anim == "noanim" then return end
    if anim and type(anim) == "function" then
        anim = self.wateranim(self.inst)
    end

	if anim and not self.inst.AnimState:IsCurrentAnimation(anim) then
        self.inst.AnimState:PlayAnimation(anim, true)
        self.inst.AnimState:SetTime(math.random())
    end

	self.inst.AnimState:SetLayer(LAYER_WORLD_BACKGROUND)
	self.inst.AnimState:SetSortOrder(-3)
    self.inst.AnimState:OverrideSymbol("water_ripple", "ripple_build", "water_ripple")
    self.inst.AnimState:OverrideSymbol("water_shadow", "ripple_build", "water_shadow")
end

function Floater:PlayThrowAnim()
    if self.showing_effect then --IsOnWater(self.inst) then
        self:PlayWaterAnim()
    else
        self:PlayLandAnim()
    end

    self.inst.AnimState:ClearOverrideSymbol("water_ripple")
    self.inst.AnimState:ClearOverrideSymbol("water_shadow")
end

function Floater:PlaySplashFx(x, y, z, tile)
    if self.splash and (not self.inst.components.inventoryitem or not self.inst.components.inventoryitem:IsHeld()) then
        local splash
        if  (tile and IsWater(tile)) or TheWorld:HasTag("island") then
            splash = SpawnPrefab("splash_water_float")
        elseif tile and IsOceanTile(tile) or tile == WORLD_TILES.IMPASSABLE then
            splash = SpawnPrefab("splash")
        end
        if splash then
            splash.Transform:SetPosition(x, y, z)
        end
    end
end

--just override this blasted thing -Half
--there are so many changes its better to simply overite it, plus this way it only checks the tile once and the events are pushed after the values are set properly
function Floater:OnLandedServer()
    local shouldfloat = self:ShouldShowEffect()
    if not self.showing_effect and shouldfloat then
        -- If something lands in a place where the water effect should be shown, and it has an inventory component,
        -- update the inventory component to represent the associated wetness.
        -- Don't apply the wetness to something held by someone, though.
        if self.inst.components.inventoryitem ~= nil and not self.inst.components.inventoryitem:IsHeld() and not self.inst:HasTag("likewateroffducksback") then
            self.inst.components.inventoryitem:AddMoisture(TUNING.OCEAN_WETNESS)
        end

        local x, y, z = self.inst.Transform:GetWorldPosition()
        local tile = TheWorld.Map:GetTileAtPoint(x, y, z)
        self:PlaySplashFx(x, y, z, tile)

        self._is_landed:set(true)
        self.showing_effect = true
        self.inst:PushEvent("floater_startfloating") --moved to after showing_effect and _is_landed so our functions have the correct information (this caused issues with obsidian tools updating anims onlanding)
        self:SwitchToFloatAnim()
    elseif self.showing_effect and not shouldfloat then --this inbred monstrosity didint support items going from water to land....

        local x, y, z = self.inst.Transform:GetWorldPosition()
        --TODO find nearby water and do a splash based on type
        --local tile = TheWorld.Map:GetTileAtPoint(x, y, z)
        self:PlaySplashFx(x, y, z)

        self._is_landed:set(false)
        self.showing_effect = false
        self.inst:PushEvent("floater_stopfloating") --moved to after showing_effect and _is_landed so our functions have the correct information

        self:SwitchToDefaultAnim()
    end
end

local _ShouldShowEffect = Floater.ShouldShowEffect
function Floater:ShouldShowEffect(...)
    local _map = TheWorld.Map
	--as of right now, the effect only runs if not on a land tile, and IA water is land to the game...
    local x, y, z = self.inst.Transform:GetWorldPosition()
    local tile = _map:GetTileAtPoint(x, y, z)
	return not TileGroupManager:IsImpassableTile(tile) and (_ShouldShowEffect(self, ...) or IsWater(GetVisualTileType(x,y,z)))
end

local _OnLandedClient = Floater.OnLandedClient
function Floater:OnLandedClient(...)
	if not self.wateranim then
		return _OnLandedClient(self, ...)
    else
        self.showing_effect = true
	end
end

--The floater component is incredibly dumb. -M
local _IsFloating = Floater.IsFloating
function Floater:IsFloating(...)
	return _IsFloating(self, ...) and not (self.inst.replica.inventoryitem and self.inst.replica.inventoryitem:IsHeld())
end


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function OnHitLand(inst)
    if inst.components.floater and inst.components.floater.landanim then
        inst.components.floater:PlayLandAnim()
    end
    --TODO add monkeyball onlanded bounce sounds
    --local bouncetime = inst.inventoryitem and inst.inventoryitem.bouncesound and inst.inventoryitem.bouncetime
    --if bouncetime then
    --    local vx, vy, vz = inst.Physics:GetVelocity()
    --    vy = vy or 0 --no idea if this even matters, but its in DS....
    --    if vy ~= 0 then
    --        if GetTime() - bouncetime > 0.15 then
    --            inst.SoundEmitter:PlaySound(self.bouncesound)
    --            inst.inventoryitem.bouncetime = GetTime()
    --        end
    --    end
    --end
end

local function OnHitWater(inst)
    if inst.components.floater and inst.components.floater.wateranim then
        inst.components.floater:PlayWaterAnim()
    end

    local isheld = inst.components.inventoryitem and inst.components.inventoryitem:IsHeld()
    --don't do this if onload or if held (in the latter case, the floater cmp is being stupid and we should probably fix the excess callbacks)
    if GetTime() > 1 and not isheld then
        --don't forget to reject all the sharx drops here
        --don't spawn sharks if the item was boat tossed, this is to prevent abuse -half
        if IsInIAClimate(inst) and not inst:HasTag("spawnnosharx") and not inst:HasTag("monstermeat") and inst.components.edible and inst.components.edible.foodtype == FOODTYPE.MEAT then
            local roll = math.random()
            local chance = TUNING.SHARKBAIT_CROCODOG_SPAWN_MULT * inst.components.edible.hungervalue
            print(inst, "Testing for crocodog/sharx:", tostring(roll) .." < ".. tostring(chance), roll<chance)
            if roll < chance then
                if math.random() < TUNING.SHARKBAIT_SHARX_CHANCE then
                    TheWorld.components.hounded:SummonSpecialSpawn(inst:GetPosition(), "sharx", math.random(TUNING.SHARKBAIT_SHARX_MIN,TUNING.SHARKBAIT_SHARX_MAX))
                else
                    TheWorld.components.hounded:SummonSpecialSpawn(inst:GetPosition(), "crocodog")
                end
            end
        end
    end
end

IAENV.AddComponentPostInit("floater", function(cmp)
    --Maybe explicitly only install the cb on master? -M
    --yes, only install on mastersim. -Z
    if TheNet:GetIsMasterSimulation() then
        cmp.inst:ListenForEvent("floater_startfloating", OnHitWater)
        cmp.inst:ListenForEvent("floater_stopfloating", OnHitLand)
    end
end)
