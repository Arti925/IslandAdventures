local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local AmphibiousCreature = require("components/amphibiouscreature")

function AmphibiousCreature:SetBuilds(land, ocean)
	self.land_build = land
	self.ocean_build = ocean
end

local _OnUpdate = AmphibiousCreature.OnUpdate
function AmphibiousCreature:OnUpdate(dt)
	--copy of original code, except it tests for SW water
	if self.inst.sg == nil or not self.inst.sg:HasStateTag("jumping") then
		local x, y, z = self.inst.Transform:GetWorldPosition()
		local is_on_water = IsOnWater(x, y, z)

		if is_on_water then
			if not self.in_water then
				self:OnEnterOcean()
			end
			return --must not run original code, lest it mistakes SW water for land
		-- elseif self.in_water then --handled by original code
			-- self:OnExitOcean()
		end
	end
	
    return _OnUpdate(self, dt)
end

function AmphibiousCreature:ShouldTransition(x, z) -- TEMP patch till water merge
	if self.in_water then
		return TheWorld.Map:IsVisualGroundAtPoint(x, 0, z) and not IsOnWater(x, 0, z)
	end

	return not TheWorld.Map:IsVisualGroundAtPoint(x, 0, z) or IsOnWater(x, 0, z)
end

local _OnEnterOcean = AmphibiousCreature.OnEnterOcean
function AmphibiousCreature:OnEnterOcean(...)
	if not self.in_water then
		if self.ocean_build then
			self.inst.AnimState:SetBuild(self.ocean_build)
		end
	end
	return _OnEnterOcean(self, ...)
end

local _OnExitOcean = AmphibiousCreature.OnExitOcean
function AmphibiousCreature:OnExitOcean(...)
	if self.in_water then
		if self.land_build then
			self.inst.AnimState:SetBuild(self.land_build)
		end
	end
	return _OnExitOcean(self, ...)
end

IAENV.AddComponentPostInit("amphibiouscreature", function(cmp)
	cmp.land_build = nil
	cmp.ocean_build = nil
    cmp.inst:AddTag("amphibious")
end)
