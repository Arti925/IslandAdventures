local COLLISION = GLOBAL.COLLISION
local TileGroups = GLOBAL.TileGroups

local old_tile_physics_init
local function new_tile_physics_init(inst, ...)
print("new_tile_physics_init", inst:HasTag("forest"))
	if inst:HasTag("forest") then
		--a slightly modified version of the forest map's primary collider.
	    inst.Map:AddTileCollisionSet(
	        COLLISION.LAND_OCEAN_LIMITS,
	        TileGroups.TransparentOceanTiles, true,
	        TileGroups.LandTiles, true,
	        0.25, 64
	    )
		--IA's ocean collider
	    inst.Map:AddTileCollisionSet(
	        COLLISION.LAND_OCEAN_LIMITS,
	        TileGroups.TEMP_LandTilesNotIAOceanTiles, true, -- TileGroups.LandTiles, true, -- TEMP: restore when waters are merged
	        TileGroups.IAOceanTiles, true,
	        0.25, 64
	    )
	    --standard impassable collider
	    inst.Map:AddTileCollisionSet(
	        COLLISION.PERMEABLE_GROUND, --maybe split permable into its own sub group in the future?
	        TileGroups.ImpassableTiles, true,
	        TileGroups.ImpassableTiles, false,
	        0.25, 128
	    )
	    return
	end
	return old_tile_physics_init ~= nil and old_tile_physics_init(inst, ...)
end

local function ShiftWaterLayers(world)
	local prefix = world:HasTag("island") and "FLAT" or "DEEP"
	GLOBAL.LAYER_WIP_BELOW_OCEAN = GLOBAL[prefix.."_LAYER_WIP_BELOW_OCEAN"]
	GLOBAL.LAYER_BELOW_GROUND = GLOBAL[prefix.."_LAYER_WIP_BELOW_OCEAN"]
end

local function installIAcomponents(inst)
	print("Loading world with IA:",inst:HasTag("forest") and "Has Forest" or "No Forest",inst:HasTag("cave") and "Has Cave" or "No Cave",inst:HasTag("island") and "Has Islands" or "No Islands", inst:HasTag("volcano") and "Has Volcano" or "No Volcano")
	if inst.ismastersim then
		if inst:HasTag("island") or inst:HasTag("volcano") then
			--inst:AddComponent('tiled')
			inst:AddComponent("worldislandtemperature")
			inst:AddComponent("volcanomanager")
			if not inst.components.lureplantspawner then
				inst:AddComponent("lureplantspawner")
			end
		end
		if inst:HasTag("island") then
			inst:AddComponent("hailrain")
			inst:AddComponent("wavemanager_ia") -- this excludes visuals, those are clientside only
			inst:AddComponent("chessnavy")
			inst:AddComponent("whalehunter")
			inst:AddComponent("tigersharker")
			inst:AddComponent("twisterspawner")
			inst:AddComponent("floodmosquitospawner")
			inst:AddComponent("rainbowjellymigration")
			inst:AddComponent("krakener")
			inst:AddComponent("buriedtreasuremanager")
		end
		if inst:HasTag("volcano") then
			for i, node in ipairs(inst.topology.nodes) do
				if node.type == "VolcanoLava" then
					if node.area == nil then
						node.area = 1
					end
					local ext = GLOBAL.ResetextentsForPoly(node.poly)

					local lava = GLOBAL.SpawnPrefab("volcanolavafx")
					lava.SetRadius(lava, 0.5 * ext.radius)
					lava.Transform:SetPosition( node.cent[1], 0, node.cent[2] )
				end
			end

			if inst.net then
				inst.net:AddComponent("volcanoambience")
			end
		end
		inst:AddComponent("doydoyspawner")
	end
	if not GLOBAL.TheNet:IsDedicated() then
		if inst:HasTag("volcano") then
			if inst.WaveComponent then
				inst:AddComponent("volcanowave")
			end
		end
		--world.components.ambientsound:SetReverbPreset("volcanolevel")
	end
	if inst:HasTag("island") then
		inst:AddComponent("flooding")
        inst.Flooding = {GetTileCenterPoint = function(self, x, y, z) return inst.components.flooding:GetTileCenterPoint(x, y, z) end} --GEOPLACEMENT SUPPORT
		GLOBAL.TileState_GroundCreep = true
		if inst.net and inst.net.components.weather then
			inst.net.components.weather.cannotsnow = true
		end
	end
	ShiftWaterLayers(inst)
	inst.installIAcomponents = nil --self-destruct after use
end

--------------------------------------------------------------------------

local WORLDTYPES = GLOBAL.WORLDTYPES or {mainclimate = {}, volcanoclimate = {}, islandclimate = {}, defaultclimate = {}, worldgen = {}}

--------------------------------------------------------------------------

AddPrefabPostInit("world", function(inst)

	--------------------------------------------------------------------------

	old_tile_physics_init = inst.tile_physics_init
	inst.tile_physics_init = new_tile_physics_init

	inst.installIAcomponents = installIAcomponents

	local OnPreLoad_old = inst.OnPreLoad
	inst.OnPreLoad = function(...)
		local primaryworldtype = inst.topology and inst.topology.overrides and inst.topology.overrides.primaryworldtype
		if not inst.topology or not inst.topology.ia_worldgen_version then primaryworldtype = "default" end --pre-RoT fix
		if primaryworldtype == nil then primaryworldtype = "default" end --RoT: Forgotten Knowledge pruned world settings for a while.

		if primaryworldtype then
			if WORLDTYPES.volcanoclimate[primaryworldtype] and not inst:HasTag("volcano") then
				inst:AddTag("volcano")
			end
			if WORLDTYPES.islandclimate[primaryworldtype] and not inst:HasTag("island") then
				inst:AddTag("island")
				local volcanoisland = inst.topology.ia_worldgen_version and inst.topology and inst.topology.overrides and inst.topology.overrides.volcanoisland or "none"
				if volcanoisland == "always" and not inst:HasTag("volcano") then
					inst:AddTag("volcano")
				end
			end
			if not WORLDTYPES.defaultclimate[primaryworldtype] and inst:HasTag("forest") then
				inst:RemoveTag("forest")
			end
		end

		if inst.installIAcomponents then
			inst:installIAcomponents()
		end

		return OnPreLoad_old and OnPreLoad_old(...)
	end

	--------------------------------------------------------------------------

end)
