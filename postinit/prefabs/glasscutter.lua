local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

IAENV.AddPrefabPostInit("glasscutter", function(inst)
    inst.hack_overridesymbols = {"swap_glasscutter", "swap_glasscutter"}
    if TheWorld.ismastersim then
        inst:AddComponent("tool")
        inst.components.tool:SetAction(ACTIONS.HACK, 3)
        inst.components.finiteuses:SetConsumption(ACTIONS.HACK, 0.5)
    end
end)

