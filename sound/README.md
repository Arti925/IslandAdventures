The source sound files are NOT included. Please be mindful not to upload hundreds of Megabytes, because everyone will have to download those to contribute to IA.

Instead, get the source files like this:

1. Extract the FMOD Sample Bank `.fsb` files in SW using a tool of your choice.
2. Place each bank into its corresponding directory in the `src` directory.
3. Open the FMOD project `.fdp` in FMOD Designer (included in mod tools on Steam).
4. Go to the tab "Banks" and click through each one to make sure all files are found. Banks missing files are marked red, and the expected filepaths are also marked red.

The `.fdp` file is a markup (=text) document. This means you can use any IDE to search-and-replace filepaths. For example, you could change all ".mp3" to ".ogg" and "/amb/volcano/" to "/amb/" if necessary. Also look up Regular Expressions for more complicated changes.

Some sounds are not from SW and included in separate directories. Uploading these is OK because they're relatively small and can't be easily gotten elsewhere.

The `src` directory should contain the following:

- amb
  - dry
  - green
  - hurricane
  - mild
  - ocean
  - rain
  - volcano
  - wet
- (custom stuff)
- music
- sfx
